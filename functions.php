<?php

add_theme_support( 'post-thumbnails' );

function playme_func($atts) {
    global $post;
    $field = $atts['video'];

    if (empty($field)) return;

    $video = get_post_meta($post->ID,  $atts['video'], true);
    $html = '
<div class="video-js-box"> 
    <video class="video-js" width="640" height="480" poster="http://video-js.zencoder.com/oceans-clip.png" controls preload> 
        <source src="'.$video.'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'> 
        <!-- Flash Fallback. -->
        <object class="vjs-flash-fallback" width="640" height="480" type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf"> 
            <param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" /> 
            <param name="allowfullscreen" value="true" /> 
            <param name="flashvars" value=\'config={"clip":{"url":"'.$video.'","autoPlay":false,"autoBuffering":true}}\' /> 
            <!-- Image Fallback --> 
            <img src="http://video-js.zencoder.com/oceans-clip.png" width="640" height="480" alt="Poster Image" title="No video playback capabilities." /> 
        </object> 
    </video> 
    <!-- Download links provided for devices that can\'t play video in the browser. --> 
    <p class="vjs-no-video"><strong>Download Video:</strong> 
        <a href="'.$video.'">MP4</a>,
        <a href="http://video-js.zencoder.com/oceans-clip.ogg">Ogg</a><br> 
        <!-- Support VideoJS by keeping this link. --> 
        <a href="http://videojs.com">HTML5 Video Player</a> by <a href="http://videojs.com">VideoJS</a>    </p> 
</div>';

    return $html;
}

add_shortcode('playme', 'playme_func');

function array_rotate ($arr, $offset)
{
    $i = 0;
    while  ($i < $offset)
    {
        $elm = array_shift($arr);
        array_push($arr, $elm);
        $i++;
    }
    return $arr;
}

function new_pat ()
{
    $chars = array('░','▒','▓','█','▀','▄');
    $i = 0;
    
    $pattern = array();     
    while ($i < rand(2,12)) {          
            array_push($pattern, $chars[array_rand($chars)]);
            $i++;
    }
    
    return $pattern;
}

function pattern_pls ($lines, $pattern, $offset) {

    $lineLen = 68;
    $patLen = sizeof($pattern);
    $totalLen = $lineLen * $lines;

    $i = 0;
    $j = 0;
    $banner = '';
    while ($i < $lines)
    {
        while ($j < 68) 
        {
            echo $pattern[$j%$patLen];
            $j++;
        }
        $pattern = array_rotate($pattern,$offset);
        $i++;
        $j = 0;
        echo '<br />';
    }
}

?>
