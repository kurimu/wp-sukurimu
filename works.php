<?php
/*
 * Template Name: works
 * */
?>
<?php get_header(); ?>
<div id="main">
    <div id="wrapper" class="clearfix">
        <div id="maincol">
            <h1>WORKS</h1>
                <?php   
                    $my_wp_query = new WP_Query();
                    $all_wp_pages = $my_wp_query->query(array(
                        'post_type' => 'page',
                        'posts_per_page' => -1
                    ));     
              
                    $portfolio =  get_page_by_title('works');
                    $portfolio_children = get_page_children($portfolio->ID, $all_wp_pages);
                    shuffle($portfolio_children);
        
                    foreach ($portfolio_children as $work) {
                        echo '<p>
                            <a href="'.$work->post_name.'">'.$work->post_title.'</a>
                            '.get_post_meta($work->ID, 'work_year', true).'
                            </p>
                            <p>
                            '.get_the_post_thumbnail($work->ID,'thumbnail').'
                            </p>';
                    }
        
                    // debug
                    echo '<pre>'.print_r($portfolio_children,true).'</pre>';

                ?>      
                <?php //echo get_post_meta(89, 'work_category'); ?>
        </div>
        
        <div id="leftcol">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
</body>
</html>
