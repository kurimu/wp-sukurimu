<?php get_header(); ?>
<div id="main">
    <div id="wrapper" class="clearfix">
        <div id="maincol">
            <?php
                $pattern = new_pat();
                $offset = rand(2,12);
                pattern_pls(5, $pattern, $offset);

                if ( have_posts())
                {
                    while ( have_posts())
                    {
                            the_post();
                            the_title('<h1 class="title">','</h1>');
                            pattern_pls(3, $pattern, $offset);
                            the_content();
                    }
                }
                else
                {
                    echo "Sorry, can't find anything";
                }

            ?>
        </div>
        <div id="leftcol">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
</body>
</html>
