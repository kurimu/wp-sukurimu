<?php get_header(); ?>
<div id="main">
    <div id="wrapper" class="clearfix">
        <div id="maincol">
            <?php
                $pattern = new_pat();
                $offset = rand(2,12);
                pattern_pls(5, $pattern, $offset);
                echo '<h1 class="ohai">su.kuri.mu</h1>';
                pattern_pls(3, $pattern, $offset);
            ?>
            <p><strong>Sorry for the <a href="http://su.kuri.mu/2010/03/29/new-site/">mess</a>!</strong></p>
            <p>I'm currently migrating bits and bytes from one server to another. I will put my projects back online soon. Meanwhile, you can check a few things that I have been busy with in the past years <a href="http://pzwart.wdka.nl/networked-media/2010/01/02/aymeric-mansoux">here</a>. If you want to be kept posted on my new projects, you can subscribe to the <a href="http://kuri.mu">KURI.MU newsletter</a>.</p>
            <p>:*</p>
            <?php
                if ( have_posts())
                {   
                    while ( have_posts())
                    {       
                            pattern_pls(5, $pattern, $offset);
                            the_post();
                            the_title('<h1 class="title">
                                        <a href="'.get_permalink().'" rel="bookmark">','</a></h1>');
                            the_time('Ymd');
                            pattern_pls(3, $pattern, $offset);
                            the_content();
                    }
                }
                else
                {   
                    echo "Sorry, can't find anything";
                }
            
            ?>

            <?php //if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php //the_time('Ymd') ?>
            <!--<a href="<?php //the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php //the_title_attribute(); ?>"><?php //the_title(); ?></a>-->
            <br />
            <?php //endwhile; else: ?>
            <p><?php //_e("Sorry, can't find anything"); ?></p> 
            <?php //endif; ?>
            <!--<h1>recent bookmarks</h1>-->
            <?php //delicious_bookmarks("320x200",6); ?>
        </div>

        <div id="leftcol">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
</body>
</html>
