<?php get_header(); ?>
<div id="main">
    <div id="wrapper" class="clearfix">
        <div id="maincol">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="post" id="post-<?php the_ID(); ?>">
                <h1 class="ohai"><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </div>
            <?php endwhile; else: ?>
            <p><?php _e("Sorry, can't find anything"); ?></p>
            <?php endif; ?>
        </div>
        <div id="leftcol">
            <?php get_sidebar(); ?>
        </div>

    </div>
</div>
</body>
</html>
